package com.banana.test.front;

import com.banana.front.pages.LoginPage;
import com.banana.front.pages.TariffContent;
import com.banana.front.pages.TariffPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.commons.text.RandomStringGenerator;

import static com.banana.front.Constants.*;
import static org.apache.commons.text.CharacterPredicates.LETTERS;
import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.assertj.core.api.Assertions.assertThat;

public class BasicFrontendTest {

    WebDriver driver;
    LoginPage loginPage;
    TariffPage tariffPage;

    private String genString(int count) {
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(LETTERS, DIGITS)
                .build();
        return generator.generate(count);
    }

    @Before
    public void manageDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);

        driver.get(AUTH_URL);
        loginPage
                .login(TELEFON, PASSWORD)
                .openTariffPeriods();
        tariffPage = new TariffPage(driver);
    }

    @Test
    public void createATariff() {
        int countBefore = tariffPage.getTariffContent().size();
        tariffPage
                .createNewTariff()
                .chooseTheLastContent();
        driver.navigate().refresh();
        assertThat(tariffPage.getTariffContent().size())
                .isEqualTo(countBefore + 1);
    }

    @Test
    public void deleteATariff() {
        int countBefore = tariffPage.getTariffContent().size();
        tariffPage.chooseTheLastContent();

        new TariffContent(driver)
                .expandMenuAndDeleteItem();
        driver.navigate().refresh();
        assertThat(tariffPage.getTariffContent().size())
                .isEqualTo(countBefore - 1);
    }

    @Test
    public void updateATariff() throws NoSuchAlgorithmException {

        String newName = NEW_TARIFF_NAME + ' ' + genString(8);
        tariffPage.chooseTheFirstContent();

        new TariffContent(driver)
                .clickTariffViewBtn()
                .updateTariffByName(newName);
        driver.navigate().refresh();

        List<String> tariffNames = new TariffPage(driver)
                .getTariffNames();
        assertThat(tariffNames).contains(newName);
    }

    @After
    public void closeBrowser() {
        driver.close();
    }
}
