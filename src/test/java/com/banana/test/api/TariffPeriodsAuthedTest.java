package com.banana.test.api;

import com.banana.api.ApiMethods;
import com.banana.api.CommonActions;
import com.banana.api.TestData;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.banana.api.Constants.*;
import static org.assertj.core.api.Assertions.*;

public class TariffPeriodsAuthedTest {

    private ApiMethods api = new ApiMethods();
    private ObjectMapper mapper = new ObjectMapper();
    private CommonActions actions = new CommonActions();
    private TestData testData = new TestData();
    private Map<String, String> newTariffRespMap;
    private String newTariffId;

    @Before
    public void getAuthTokenAndCreateNewTariff() throws IOException {

        HttpResponse httpResponse = api.sendPost(API_URL + AUTH_BASIC, testData.getAuthData());
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);

        Map<String, String> responseMap = mapper.readValue(httpResponse.getEntity().getContent(), Map.class);
        BasicHeader token = new BasicHeader("set-cookie",
                "X-Authorization=" + responseMap.get("token"));

        HttpResponse resp = api.sendPost(API_URL + TARIFF_PERIOD, testData.getTariffData(), token);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);
        newTariffRespMap = mapper.readValue(resp.getEntity().getContent(), Map.class);
        newTariffId = newTariffRespMap.get("id");
    }

    @Test
    public void checkNewTariffInTheList() throws IOException {

        HttpResponse resp = api.sendGet(API_URL + TARIFF_PERIODS_LIST);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);
        List<HashMap> tariffList = mapper.readValue(resp.getEntity().getContent(), List.class);

        Map<String, String> tariffInList = actions.findTheTariffInList(tariffList, newTariffId);
        assertThat(newTariffRespMap)
                .containsAllEntriesOf(tariffInList);
    }

    @Test
    public void getTheTariffByIdUpdateItAndCheckUpdatesInList() throws IOException {

        HttpResponse getTariffByIdResp = api.sendGet(API_URL + TARIFF_PERIOD + newTariffId);
        Map<String, String> getRespMap = mapper.readValue(getTariffByIdResp.getEntity().getContent(), Map.class);
        assertThat(getTariffByIdResp.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);
        assertThat(newTariffRespMap)
                .containsAllEntriesOf(getRespMap);

        HttpResponse putResp = api.sendPut(API_URL + TARIFF_PERIOD + newTariffId,
                testData.getUpdatedTariffData());
        Map<String, String> updatedTariff = mapper.readValue(putResp.getEntity().getContent(), Map.class);
        assertThat(putResp.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);

        HttpResponse getTariffList = api.sendGet(API_URL + TARIFF_PERIODS_LIST);
        List<HashMap> tariffList = mapper.readValue(getTariffList.getEntity().getContent(), List.class);
        Map<String, String> updatedTariffInTheList = actions.findTheTariffInList(tariffList, newTariffId);
        assertThat(updatedTariff)
                .containsAllEntriesOf(updatedTariffInTheList);
    }

    @After
    public void deleteNewTariff() throws  IOException {
        HttpResponse delResp = api.sendDelete(API_URL + TARIFF_PERIOD + newTariffId);
        assertThat(delResp.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);
    }

}
