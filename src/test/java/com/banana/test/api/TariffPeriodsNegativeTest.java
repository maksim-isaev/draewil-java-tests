package com.banana.test.api;

import com.banana.api.ApiMethods;
import com.banana.api.TestData;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.banana.api.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TariffPeriodsNegativeTest {

    private ApiMethods api = new ApiMethods();
    private ObjectMapper mapper = new ObjectMapper();
    private TestData testData = new TestData();
    private Map<String, String> tariffInListMap;
    private String tariffInListId;

    @Before
    public void getAuthTokenAndTakeTariffFromTheList() throws IOException {

        HttpResponse httpResponse = api.sendPost(API_URL + AUTH_BASIC, testData.getAuthData());
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(STATUS_OK);

        Map<String, String> responseMap = mapper.readValue(httpResponse.getEntity().getContent(), Map.class);
        BasicHeader token = new BasicHeader("set-cookie",
                "X-Authorization=" + responseMap.get("token"));

        HttpResponse getListResp = api.sendGet(API_URL + TARIFF_PERIODS_LIST, token);
        List<HashMap> tariffList = mapper.readValue(getListResp.getEntity().getContent(), List.class);
        tariffInListMap = tariffList.get(0);
        tariffInListId = tariffInListMap.get("id");
    }

    @Test
    public void tryToCreateTariffBadRequest() throws IOException {
        HttpResponse postResp = api.sendPost(API_URL + TARIFF_PERIOD, "");
        assertThat(postResp.getStatusLine().getStatusCode())
                .isEqualTo(BAD_REQUEST);
    }

    @Test
    public void tryToUpdateTariffBadRequest() throws  IOException {
        HttpResponse resp = api.sendPut(API_URL + TARIFF_PERIOD + tariffInListId, "");
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(BAD_REQUEST);
    }
}
