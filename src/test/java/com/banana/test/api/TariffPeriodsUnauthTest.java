package com.banana.test.api;

import com.banana.api.ApiMethods;
import com.banana.api.TestData;
import org.apache.http.HttpResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.banana.api.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TariffPeriodsUnauthTest {

    private ApiMethods api = new ApiMethods();
    private ObjectMapper mapper = new ObjectMapper();
    private TestData testData = new TestData();

    private Map getUnauthMessage() {
        Map<String, String> unauthMessage = new HashMap<String, String>();
        unauthMessage.put("message", "Unauthorized!");
        return unauthMessage;
    }

    @Test
    public void tryToGetTariffList() throws IOException{
        HttpResponse resp = api.sendGet(API_URL + TARIFF_PERIODS_LIST);
        Map<String, String> map = mapper.readValue(resp.getEntity().getContent(), Map.class);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(UNAUTHORIZED);
        assertThat(map).containsAllEntriesOf(getUnauthMessage());
    }

    @Test
    public void tryToCreateTariff() throws IOException {
        HttpResponse resp = api.sendPost(API_URL + TARIFF_PERIOD, testData.getTariffData());
        Map<String, String> map = mapper.readValue(resp.getEntity().getContent(), Map.class);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(UNAUTHORIZED);
        assertThat(map).containsAllEntriesOf(getUnauthMessage());
    }

    @Test
    public void tryToUpdateTariff() throws IOException {
        HttpResponse resp = api.sendPut(API_URL + TARIFF_PERIOD + TARIFF_ID, testData.getTariffData());
        Map<String, String> map = mapper.readValue(resp.getEntity().getContent(), Map.class);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(UNAUTHORIZED);
        assertThat(map).containsAllEntriesOf(getUnauthMessage());
    }

    @Test
    public void tryToDeleteTariff() throws IOException {
        HttpResponse resp = api.sendDelete(API_URL + TARIFF_PERIOD + TARIFF_ID);
        Map<String, String> map = mapper.readValue(resp.getEntity().getContent(), Map.class);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(UNAUTHORIZED);
        assertThat(map).containsAllEntriesOf(getUnauthMessage());
    }

    @Test
    public void tryToGetTariffById() throws IOException {
        HttpResponse resp = api.sendGet(API_URL + TARIFF_PERIOD + TARIFF_ID);
        Map<String, String> map = mapper.readValue(resp.getEntity().getContent(), Map.class);
        assertThat(resp.getStatusLine().getStatusCode())
                .isEqualTo(UNAUTHORIZED);
        assertThat(map).containsAllEntriesOf(getUnauthMessage());
    }
}
