package com.banana.front.selectors;

import org.openqa.selenium.By;

public class CreateViewTariffPageSelectors {

    private By tariffName = new By.ByXPath(".//input[@name='name']");
    private By dropFrom = new By.ByXPath(".//select[@name='from']");
    private By dropTo = new By.ByXPath(".//select[@name='to']");
    private By checkboxes = new By.ByXPath(".//label");
    private By submitBtn = new By.ByXPath(".//button[@type='submit']");
    private By backBtn = new By.ByXPath(".//button[text()='Back']");
    private By editBtn = new By.ByXPath(".//button[text()='Edit']");

    public By getTariffName() {
        return tariffName;
    }

    public By getDropFrom() {
        return dropFrom;
    }

    public By getDropTo() {
        return dropTo;
    }

    public By getCheckboxes() {
        return checkboxes;
    }

    public By getSubmitBtn() {
        return submitBtn;
    }

    public By getBackBtn() {
        return backBtn;
    }

    public By getEditBtn() {
        return editBtn;
    }
}
