package com.banana.front.selectors;

import org.openqa.selenium.By;

public class DeleteContentSelectors {

    private By code = new By.ByXPath(".//h3");
    private By inputCode = new By.ByXPath(".//input");
    private By deleteBtn = new By.ByXPath(".//button[text()='Delete']");

    public By codeValue() {
        return code;
    }

    public By getInputCode() {
        return inputCode;
    }

    public By getDeleteBtn() {
        return deleteBtn;
    }
}
