package com.banana.front.selectors;

import org.openqa.selenium.By;

public class TariffsPageSelectors {

    private By newTariffBtn = new By.ByXPath(".//a[@class='o-button o-button--deemphasised']");
    private By tariffContents = new By.ByXPath(".//div[@class='c-entity-summary_content']");

    public By getNewTariffBtn() {
        return newTariffBtn;
    }

    public By getTariffContents() {
        return tariffContents;
    }

}
