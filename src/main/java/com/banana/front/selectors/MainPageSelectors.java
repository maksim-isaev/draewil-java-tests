package com.banana.front.selectors;

import org.openqa.selenium.By;

public class MainPageSelectors {

    private By menuBtn = new By.ByXPath(".//button[@class='o-icon-button fa fa-bars c-sidebar_button']");
    private By menuBtnClose = new By.ByXPath(".//button[@class='o-icon-button fa fa-bars']");
    private By logisticsLink = new By.ByXPath(".//span[text()='Logistics']");
    private By tariffPeriodsLink = new By.ByXPath(".//a[text()[contains(.,'Tariff Periods')]]");

    public By getLogisticsLink() {
        return logisticsLink;
    }

    public By getTariffPeriodsLink() {
        return tariffPeriodsLink;
    }

    public By getMenuBtn() {
        return menuBtn;
    }

    public By getMenuBtnClose() {
        return menuBtnClose;
    }
}
