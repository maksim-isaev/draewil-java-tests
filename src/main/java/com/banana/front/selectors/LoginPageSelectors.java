package com.banana.front.selectors;

import org.openqa.selenium.By;

public class LoginPageSelectors {

    private By telefon = new By.ByXPath(".//input[@name='phonePhoneNumber']");
    private By password = new By.ByXPath(".//input[@name='password']");
    private By subnitBtn = new By.ByXPath(".//div[@class='o-form_actions ng-scope']");

    public By getTelefon() {
        return telefon;
    }

    public By getPassword() {
        return password;
    }

    public By getSubnitBtn() { return subnitBtn; }
}
