package com.banana.front.selectors;

import org.openqa.selenium.By;

public class TariffContentSelectors {

    private By tariffViewBtn = new By.ByXPath(".//button[@class='o-button c-dropdown-navigation_primary-action ng-binding ng-scope']");
    private By tariffMenuExpand = new By.ByXPath(".//button[@class='o-button c-dropdown-navigation_toggle-button']");
    private By deleteTariffBtn = new By.ByXPath(".//div[@class='c-dropdown-navigation_options ng-scope']");
    private By tariffTitle = new By.ByXPath(".//h2[@class='o-heading-large c-entity-summary_name ng-binding']");

    public By getTariffViewBtn() {
        return tariffViewBtn;
    }

    public By getTariffMenuExpand() {
        return tariffMenuExpand;
    }

    public By getDeleteTariffBtn() {
        return deleteTariffBtn;
    }

    public By getTariffTitle() { return tariffTitle;
    }
}
