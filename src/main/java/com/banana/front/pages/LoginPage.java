package com.banana.front.pages;

import com.banana.front.selectors.LoginPageSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends CommonPage {

    private LoginPageSelectors pageSelectors;

    public LoginPage(WebDriver driver) {
        super(driver);
        pageSelectors = new LoginPageSelectors();
    }

    private void fillTelefon(String telefon) {
        wait.until(ExpectedConditions.elementToBeClickable(pageSelectors.getTelefon()))
                .sendKeys(telefon);
    }

    private void fillPassword(String pass) {
        wait.until(ExpectedConditions.elementToBeClickable(pageSelectors.getPassword()))
                .sendKeys(pass);
    }

    private MainPage clickSubmit() {
        wait.until(ExpectedConditions.elementToBeClickable(pageSelectors.getSubnitBtn()))
                .click();
        return new MainPage(driver);
    }

    public MainPage login(String telefon, String password) {
        fillTelefon(telefon);
        fillPassword(password);
        return clickSubmit();
    }
}
