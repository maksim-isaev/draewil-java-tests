package com.banana.front.pages;

import com.banana.front.selectors.TariffContentSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TariffContent extends CommonPage {

    private TariffContentSelectors content;

    public TariffContent(WebDriver driver) {
        super(driver);
        content = new TariffContentSelectors();
    }

    private DeleteContent clickDeleteTariffBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(content.getDeleteTariffBtn()))
                .click();
        return new DeleteContent(driver);
    }

    private void clickMenuExpandBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(content.getTariffMenuExpand()))
                .click();
    }

    public CreateViewTariffPage clickTariffViewBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(content.getTariffViewBtn()))
                .click();
        return new CreateViewTariffPage(driver);
    }

    public TariffPage expandMenuAndDeleteItem() {
        clickMenuExpandBtn();
        return clickDeleteTariffBtn().deleteTariff();
    }
    public String getTariffName() {
        return wait.until(ExpectedConditions.elementToBeClickable(content.getTariffTitle()))
                .getText();
    }
}
