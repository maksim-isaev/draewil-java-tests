package com.banana.front.pages;

import com.banana.front.selectors.DeleteContentSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteContent extends CommonPage {

    private DeleteContentSelectors selectors;

    public DeleteContent(WebDriver driver) {
        super(driver);
        selectors = new DeleteContentSelectors();
    }

    private String getCode() {
        return wait.until(ExpectedConditions.elementToBeClickable(selectors.codeValue()))
                .getText();
    }

    private void putCode() {
        wait.until(ExpectedConditions.elementToBeClickable(selectors.getInputCode()))
                .sendKeys(getCode());
    }

    private void clickDeleteBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(selectors.getDeleteBtn()))
                .click();
    }

    public TariffPage deleteTariff() {
        putCode();
        clickDeleteBtn();
        return new TariffPage(driver);
    }
}
