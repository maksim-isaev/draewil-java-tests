package com.banana.front.pages;

import com.banana.front.selectors.CreateViewTariffPageSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static com.banana.front.Constants.NEW_TARIFF_NAME;

public class CreateViewTariffPage extends CommonPage {

    private CreateViewTariffPageSelectors createViewTariffPageSelectors;

    public CreateViewTariffPage(WebDriver driver) {
        super(driver);
        createViewTariffPageSelectors = new CreateViewTariffPageSelectors();
    }

    private WebElement getNameInput() {
        return wait.until(ExpectedConditions
                .elementToBeClickable(createViewTariffPageSelectors.getTariffName()));
    }

    private void putName(String name) {
        getNameInput().sendKeys(name);
    }

    private void clearName() {
        getNameInput().clear();
    }

    private void chooseSomething(WebElement el) {
        Select sel = new Select(el);
        sel.selectByIndex(1);
    }

    private void chooseFromDropdown() {
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(driver
                .findElement(createViewTariffPageSelectors.getDropFrom())));
        chooseSomething(el);
    }
    private void chooseToDropdown() {
        WebElement el = wait.until(ExpectedConditions.elementToBeClickable(driver
                .findElement(createViewTariffPageSelectors.getDropTo())));
        chooseSomething(el);
    }

    private List<WebElement> getCheckboxes() {
        List<WebElement> list = driver.findElements(createViewTariffPageSelectors.getCheckboxes());
        return wait.until(ExpectedConditions.visibilityOfAllElements(list));
    }

    private void selectCheckboxes() {
        List<WebElement> list = getCheckboxes();
        for (WebElement el : list) {
            el.click();
        }
    }

    private void clickSubmitBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(createViewTariffPageSelectors.getSubmitBtn()))
                .click();
    }

    private void clickBackButton() {
        wait.until(ExpectedConditions.elementToBeClickable(createViewTariffPageSelectors.getBackBtn()))
                .click();
    }

    private void clickEditBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(createViewTariffPageSelectors.getEditBtn()))
                .click();
    }

    public TariffPage fillTheForm() {
        putName(NEW_TARIFF_NAME);
        chooseFromDropdown();
        chooseToDropdown();
        selectCheckboxes();
        clickSubmitBtn();
        return new TariffPage(driver);
    }

    public TariffPage pressBack() {
        clickBackButton();
        return  new TariffPage(driver);
    }

    public TariffPage updateTariffByName(String newName) {
        clickEditBtn();
        clearName();
        putName(newName);
        clickSubmitBtn();
        clickBackButton();
        return new TariffPage(driver);
    }
}
