package com.banana.front.pages;

import com.banana.front.selectors.TariffContentSelectors;
import com.banana.front.selectors.TariffsPageSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class TariffPage extends CommonPage {
    private TariffsPageSelectors tariffsPageSelectors;
    private TariffContentSelectors tariffContentSelectors;

    public TariffPage(WebDriver driver) {
        super(driver);
        tariffsPageSelectors = new TariffsPageSelectors();
        tariffContentSelectors = new TariffContentSelectors();
    }

    private CreateViewTariffPage clickNewTariffBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(tariffsPageSelectors.getNewTariffBtn()))
                .click();
        return  new CreateViewTariffPage(driver);
    }

    public TariffPage createNewTariff() {
        return clickNewTariffBtn().fillTheForm();
    }

    public List<WebElement> getTariffContent() {
        return wait.until(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(tariffsPageSelectors.getTariffContents()));
    }

    public WebElement chooseTheLastContent() {
        List<WebElement> tariffList = getTariffContent();
        return tariffList.get(tariffList.size() - 1);
    }

    public WebElement chooseTheFirstContent() {
        List<WebElement> tariffList = getTariffContent();
        return tariffList.get(0);
    }

    public List<String> getTariffNames() {
        List<String> names = new ArrayList<>();
        for (WebElement el : getTariffContent()) {
            names.add(el.findElement(tariffContentSelectors.getTariffTitle()).getText());
        }
        return names;
    }

}
