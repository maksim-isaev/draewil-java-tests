package com.banana.front.pages;

import com.banana.front.selectors.MainPageSelectors;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainPage extends CommonPage {

    private MainPageSelectors mainPageSelectors;

    public MainPage(WebDriver driver) {
        super(driver);
        mainPageSelectors = new MainPageSelectors();
    }

    private void clickLogisticLink() {
        wait.until(ExpectedConditions.elementToBeClickable(mainPageSelectors.getLogisticsLink()))
                .click();
    }

    private void clickTariffsLink() {
        wait.until(ExpectedConditions.elementToBeClickable(mainPageSelectors.getTariffPeriodsLink()))
                .click();
    }

    private void clickMenuBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(mainPageSelectors.getMenuBtn()))
                .click();
    }

    private void clickMenuCloseBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(mainPageSelectors.getMenuBtnClose()))
                .click();
    }

    public TariffPage openTariffPeriods() {
        clickMenuBtn();
        clickLogisticLink();
        clickTariffsLink();
        clickMenuCloseBtn();
        return new TariffPage(driver);
    }
}

