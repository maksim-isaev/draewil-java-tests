package com.banana.api;

public class Constants {

    public static final String API_URL = "https://preprod-api.draewil.net/v1/";
    public static final String AUTH_BASIC = "auth/basic";
    public static final String TARIFF_PERIODS_LIST = "tariffPeriods";
    public static final String TARIFF_PERIOD = "tariffPeriod/";
    public static final String LOREM = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    public static final String TARIFF_ID = "5bbe25ef-2c87-41d8-9a40-c01fa7643541";

    public static final String UTF_8 = "UTF-8";

    public static final int STATUS_OK = 200;
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZED = 401;

    public static final String BAD_REQUEST_RESPONSE = "{message=Bad request, validationErrors={name=[{errorCode=REQUIRED, errorMessage=Required}], days=[{errorCode=REQUIRED, errorMessage=Required}]}}";

}
