package com.banana.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonActions {

    public Map<String, String> findTheTariffInList(List<HashMap> tariffList, String tariffId) {
        for (HashMap tariff: tariffList) {
            if(tariff.get("id").equals(tariffId)) {
                return tariff;
            }
        }
        return null;
    }
}
