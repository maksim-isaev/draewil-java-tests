package com.banana.api;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class ApiMethods {

    private HttpClient httpClient;

    public ApiMethods() {
        this.httpClient = HttpClientBuilder.create().build();
    }

    private HttpResponse sendRequest(HttpRequestBase request, Header token) throws IOException {
        if (token != null) {
            request.addHeader(token);
        };
        request.addHeader("content-type", "application/json");
         return httpClient.execute(request);
    }

    private HttpResponse sendRequest(HttpEntityEnclosingRequestBase request, StringEntity params, Header token)
            throws IOException {
        if (token != null) {
            request.addHeader(token);
        };
        request.addHeader("content-type", "application/json");
        request.setEntity(params);
        return httpClient.execute(request);
    }

    public HttpResponse sendPost(String uri, String data,  Header token) throws IOException {
        HttpPost request = new HttpPost(uri);
        StringEntity params = new StringEntity(data,"UTF-8");
        return sendRequest(request, params, token);
    }

    public HttpResponse sendPost(String uri, String data) throws IOException {
        return sendPost(uri, data, null);
    }

    public HttpResponse sendGet(String uri, Header token) throws IOException {
        HttpGet request = new HttpGet(uri);
        return sendRequest(request, token);
    }

    public HttpResponse sendGet(String uri) throws IOException {
        return sendGet(uri, null);
    }

    public HttpResponse sendPut(String uri, String data,  Header token) throws IOException {
        HttpPut request = new HttpPut(uri);
        StringEntity params = new StringEntity(data,"UTF-8");
        return sendRequest(request, params, token);
    }

    public HttpResponse sendPut(String uri, String data) throws IOException {
        return sendPut(uri, data, null);
    }

    public HttpResponse sendDelete(String uri, Header token) throws IOException {
        HttpDelete request = new HttpDelete(uri);
        return sendRequest(request, token);
    }

    public HttpResponse sendDelete(String uri) throws IOException {
        return sendDelete(uri, null);
    }

}
