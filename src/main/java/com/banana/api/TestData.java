package com.banana.api;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.banana.api.Constants.LOREM;

public class TestData {

    private ObjectMapper mapper;

    public TestData() {
        this.mapper = new ObjectMapper();
    }

    private Map<String, String> genDays(boolean flag) {
        Map<String, String> daysMap = new HashMap<String, String>();
        for (int i = 1; i <= 7 ; i++) {
            daysMap.put(Integer.toString(i), Boolean.toString(flag));
        }
        return daysMap;
    }

    public String getAuthData() throws IOException {
        Map<String, String> authData = new HashMap<String, String>();
        authData.put("password", "TestTask");
        authData.put("phoneNumber", "3330222");
        authData.put("countryDiallingCode", "965");
        return this.mapper.writeValueAsString(authData);
    }

    public String getTariffData(boolean flag) throws IOException {
        Map<String, String> tariffData = new HashMap<String, String>();
        tariffData.put("from", "00:00");
        tariffData.put("to", "23:30");
        tariffData.put("name", "Tariff created by API");
        tariffData.put("days", genDays(flag).toString());
        return this.mapper.writeValueAsString(tariffData);
    }

    public String getTariffData() throws IOException {
        return getTariffData(true);
    }

    public String getUpdatedTariffData() throws IOException {
        Map<String, String> updatedTariffData = mapper.readValue(getTariffData(), Map.class);
        updatedTariffData.put("from", "00:30");
        updatedTariffData.put("to", "23:00");
        updatedTariffData.put("name", "Tariff updated by API");
        return this.mapper.writeValueAsString(updatedTariffData);
    }

    public String getUpdatedTariffDataNameLonger() throws IOException {
        Map<String, String> updatedTariffData = mapper.readValue(getTariffData(), Map.class);
        updatedTariffData.put("name", LOREM);
        return this.mapper.writeValueAsString(updatedTariffData);
    }

}